package com.arvs.ebikes.loginregissqlite;

public class ModelRegis_form {


    private int user_id;
    private String user_name;
    private int user_contact_no;
    private String email_id;
    private String password;


    public ModelRegis_form() {
    }

    public ModelRegis_form(int user_id, String user_name, int user_contact_no, String email_id, String password) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.user_contact_no = user_contact_no;
        this.email_id = email_id;
        this.password = password;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getUser_contact_no() {
        return user_contact_no;
    }

    public void setUser_contact_no(int user_contact_no) {
        this.user_contact_no = user_contact_no;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
