package com.arvs.ebikes.loginregissqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private ModelRegis_form modelRegis_form;
    private Context context;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "login_form";
    public static final String TABLE_NAME = "registration_table";
    public static final String COL1 = "user_id";
    public static final String COL2 = "user_name";
    public static final String COL3 = "user_contact_no";
    public static final String COL4 = "email_id";
    public static final String COL5 = "password";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = "CREATE TABLE " + TABLE_NAME + "(user_id INTEGER PRIMARY KEY AUTOINCREMENT,user_name TEXT,user_contact_no INTEGER,email_id TEXT,password TEXT);";

        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }


    public void insertUserRecord(ModelRegis_form records) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COL1, records.getUser_id());
        values.put(COL2, records.getUser_name());
        values.put(COL3, records.getUser_contact_no());
        values.put(COL4, records.getEmail_id());
        values.put(COL5, records.getPassword());

        sqLiteDatabase.insert(TABLE_NAME, null, values);
        sqLiteDatabase.close();
    }

    public void updateUserRecord(ModelRegis_form updateRecord) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COL2, updateRecord.getUser_name());
        values.put(COL3, updateRecord.getUser_contact_no());
        values.put(COL4, updateRecord.getEmail_id());
        values.put(COL5, updateRecord.getPassword());


        sqLiteDatabase.update(TABLE_NAME, values, COL1 + "=?", new String[]{String.valueOf(updateRecord.getUser_id())});
        sqLiteDatabase.close();

    }

    public void deleteUserRecord(int id) {

        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, COL1 + "=?", new String[]{String.valueOf(id)});
        db.close();


    }

    public List<ModelRegis_form> getAllUserRecordsList() {

        SQLiteDatabase database = getReadableDatabase();
        ArrayList<ModelRegis_form> recordsList = new ArrayList<ModelRegis_form>();

        String query = "SELECT * FROM " + TABLE_NAME;

        Cursor cursor = database.rawQuery(query, null);


        if (cursor.moveToFirst()) {
            do {
                ModelRegis_form records = new ModelRegis_form();
                records.setUser_id(cursor.getInt(0));
                records.setUser_name(cursor.getString(1));
                records.setUser_contact_no(cursor.getInt(2));
                records.setEmail_id(cursor.getString(3));
                records.setPassword(cursor.getString(4));

                recordsList.add(records);

            } while (cursor.moveToNext());

        }
        return recordsList;
    }


}
