package com.arvs.ebikes.loginregissqlite;

import android.content.Intent;
import android.os.Parcelable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextInputEditText user_name, contact_no, UserId, registration_password;
    private TextView register_btn, alreadyUser_textView;
    private ProgressBar progressBar;

    private DatabaseHelper myDb;
    private List<ModelRegis_form> list;
    private ArrayList<ModelRegis_form> arrayList=new ArrayList<ModelRegis_form>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myDb = new DatabaseHelper(this);

        user_name = findViewById(R.id.user_name);
        contact_no = findViewById(R.id.contact_no);
        UserId = findViewById(R.id.UserId);
        registration_password = findViewById(R.id.registration_password);
        register_btn = findViewById(R.id.register_btn);
        alreadyUser_textView = findViewById(R.id.alreadyUser_textView);
        progressBar = findViewById(R.id.progressBar);


        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userName=user_name.getText().toString();
                int contactNumber= Integer.parseInt(contact_no.getText().toString());
                String emailId=UserId.getText().toString();
                String password=registration_password.getText().toString();

                ModelRegis_form regis_form=new ModelRegis_form();
                regis_form.setUser_name(userName);
                regis_form.setUser_contact_no(contactNumber);
                regis_form.setEmail_id(emailId);
                regis_form.setPassword(password);

                myDb.insertUserRecord(regis_form);
                Toast.makeText(MainActivity.this, "Data Saved to database", Toast.LENGTH_SHORT).show();

                startActivity(new Intent(MainActivity.this,DataBaseRecordActivity.class)
                        .putExtra("mydata", (Parcelable) regis_form));





            }
        });






    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
