package com.arvs.ebikes.loginregissqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class DataBaseRecordActivity extends AppCompatActivity {

    private RecyclerView myRecyclerView;
    private ArrayList<ModelRegis_form> arrayList=new ArrayList<ModelRegis_form>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_base_record);

        myRecyclerView=findViewById(R.id.myRecyclerView);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(DataBaseRecordActivity.this);


        DataBaseAdapter adapter=new DataBaseAdapter(arrayList,DataBaseRecordActivity.this);
        myRecyclerView.setAdapter(adapter);
    }


    public ArrayList<ModelRegis_form> myList(){

//        ModelRegis_form
        return arrayList;
    }
}
