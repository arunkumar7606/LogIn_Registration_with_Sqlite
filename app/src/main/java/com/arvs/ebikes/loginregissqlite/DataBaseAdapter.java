package com.arvs.ebikes.loginregissqlite;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class DataBaseAdapter extends RecyclerView.Adapter<DataBaseAdapter.MyViewHolder> {

    private ArrayList<ModelRegis_form> arrayList;
    private Context context;
    private LayoutInflater layoutInflater;

    public DataBaseAdapter() {
    }

    public DataBaseAdapter(ArrayList<ModelRegis_form> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=layoutInflater.inflate(R.layout.my_view,null,false);

        DataBaseAdapter.MyViewHolder myViewHolder=new DataBaseAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.text_user_name.setText(arrayList.get(position).getUser_name());
        holder.text_user_contact.setText(arrayList.get(position).getUser_contact_no());
        holder.text_user_email.setText(arrayList.get(position).getEmail_id());
        holder.text_user_password.setText(arrayList.get(position).getPassword());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView text_user_name,text_user_contact,text_user_email,text_user_password;
        public MyViewHolder(View itemView) {

            super(itemView);

            text_user_name= itemView.findViewById(R.id.text_user_name);
            text_user_contact= itemView.findViewById(R.id.text_user_contact);
            text_user_email= itemView.findViewById(R.id.text_user_email);
            text_user_password= itemView.findViewById(R.id.text_user_password);

        }
    }
}
